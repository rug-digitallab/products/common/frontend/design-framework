import { create } from "@storybook/theming/create";

export default create({
    base: "light",
    brandTitle: "Digital Lab Design Framework",
    brandUrl: "rug.nl",
    brandImage: "/assets/rug.png",
    brandTarget: "_self",

    colorPrimary: "#dc002d",
    colorSecondary: "#009CEF",

    appPreviewBg: "#EEEEEE",
})
