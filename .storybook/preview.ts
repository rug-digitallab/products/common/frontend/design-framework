import { setCustomElementsManifest, type Preview } from "@storybook/web-components";
import "../src/style/uno.css";
import "@storybook/addon-console";

// Generate metadata for our web components
import manifest from "../custom-elements.json";
setCustomElementsManifest(manifest);

const preview: Preview = {
    parameters: {
        controls: {
            matchers: {
                color: /(background|color)$/i,
                date: /Date$/i,
            },
        },
    },
};

export default preview;
