/**
 * Value specifications for Design Tokens.
*/
export const tokens = {
    // Color
    NEUTRAL: "#F0F3FA",
    NEUTRAL_CONTRAST: "#FFFFFF",
    ACCENT: "#3A405A",
    ACCENT_CONTRAST: "#242635",
    SUCCESS: "#AEDF70",
    SUCCESS_SECONDARY: "#3F8813",
    SUCCESS_CONTRAST: "#11CC00",
    FAILED: "#EA7359",
    FAILED_SECONDARY: "#7C1111",
    FAILED_CONTRAST: "#E01951",
    PENDING: "#F8D889",
    PENDING_SECONDARY: "#B0820A",
    PENDING_CONTRAST: "#F4B701",

    // Font size
    LARGE: "3rem",
    MEDIUM: "2rem",
    SMALL: "1rem",
};
