import { LitElement } from "lit";

/**
 * Base class for Digital Lab - Lit Web Components. Extends `LitElement`.
 *
 * The generic `T` must be the type of the inheriting component. This enforces
 * correct object initialization. For example:
 *
 * ```
 * export default class MyElement extends DLElement<MyElement> { ... }
 * ```
 *
 * Can then be instantiated like:
 *
 * ```
 * myElement = new MyElement({ propertyName: propertyValue, ... })
 * ```
 */
export default abstract class DLElement<T> extends LitElement {
    private _init?: Partial<T>;

    public constructor(init?: Partial<T>) {
        super();
        this._init = init;
    }

    /* Object.assign in constructor does not trigger an update.
    It must be done after the automatic initial update. */
    protected firstUpdated(): void {
        Object.assign(this, this._init);
    }
}
