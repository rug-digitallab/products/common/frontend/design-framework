# Molecules

![Digital Lab Design Framework Banner](../../../assets/organism_banner.png)

Organisms are the biggest UI components, and they often group a series of responsibilites that can or not be loosely connected. The best way to identify an organism is by their position within the structure of the design, since they still maintain clear boundaries from the rest of the components.

For example, a header bar may have a logo, navigation buttons, a search box and a hamburger menu. All of these are independent *atoms* or *molecules*, yet they have an underlying common purpose: present the user with essential information. Additionally, they are commonly separated from the rest of the system with a clear design-wise boundary.
