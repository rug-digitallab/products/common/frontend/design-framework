import { html } from "lit";
import type { Meta, StoryObj } from "@storybook/web-components";

import "./SubmissionTimeline";
import "../submission-card/SubmissionCard";
import "../../atom/content/status-badge/StatusBadge";
import "../../molecule/submission-stage/SubmissionStage";

export default {
    title: "Organism/SubmissionTimeline",
    component: "dl-submission-timeline",
    tags: ["autodocs"],
} as Meta;

export const NewDefault: StoryObj = {
    args: {
        timelineEntries: [
            {
                timestamp: "Monday",
                statusBadge: html`
                    <dl-status-badge
                        status="Success"
                    ></dl-status-badge>
                `,
                submission: html`
                    <dl-submission-card
                        submissionName="Submission #1"
                        .stages=${[
                            html`
                                <dl-submission-stage stageName="Only one stage">
                                    <div>
                                        This first submission has only one inner stage.
                                    </div>
                                </dl-submission-stage>
                            `,
                        ]}
                    ></dl-submission-card>
                `,
            },
            {
                timestamp: "Tuesday",
                statusBadge: html`
                    <dl-status-badge
                        status="Success"
                    ></dl-status-badge>
                `,
                submission: html`
                    <dl-submission-card
                        submissionName="Submission #2"
                        .stages=${[
                            html`
                                <dl-submission-stage stageName="First Stage">
                                    <div>
                                        Here there are two stages, this is the first
                                        one.
                                    </div>
                                </dl-submission-stage>
                            `,
                            html`
                                <dl-submission-stage stageName="Second Stage">
                                    <div>
                                        Here there are two stages, this is the second
                                        one.
                                    </div>
                                </dl-submission-stage>
                            `,
                        ]}
                    ></dl-submission-card>
                `,
            },
            {
                timestamp: "Yesterday",
                statusBadge: html`
                    <dl-status-badge
                        status="Failed"
                    ></dl-status-badge>
                `,
                submission: html`
                    <dl-submission-card
                        submissionName="Submission #3"
                        .stages=${[
                            html`
                                <dl-submission-stage stageName="Failed Submission">
                                    <div>
                                        This submission has failed, the status badge on
                                        the left displays this information.
                                    </div>
                                </dl-submission-stage>
                            `,
                            ,
                        ]}
                    ></dl-submission-card>
                `,
            },
            {
                timestamp: "3 min ago",
                statusBadge: html`
                    <dl-status-badge
                        status="Pending"
                    ></dl-status-badge>
                `,
                submission: html`
                    <dl-submission-card
                        submissionName="Submission #4"
                        .stages=${[
                            html`
                                <dl-submission-stage
                                    stageName="Running Submission"
                                >
                                    <div>This recent submission is still running.</div>
                                </dl-submission-stage>
                            `,
                            ,
                        ]}
                    ></dl-submission-card>
                `,
            },
        ],
    },
};
