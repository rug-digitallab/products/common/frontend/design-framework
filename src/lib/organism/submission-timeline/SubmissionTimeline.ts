import { css, html } from "lit";
import { customElement, property } from "lit/decorators.js";

import DLElement from "../../DLElement";
import { SubmissionTimelineEntry } from "../../model/TimelineEntry";

/**
 * Vertical timeline of ordered submissions.
 */
@customElement("dl-submission-timeline")
export default class SubmissionTimeline extends DLElement<SubmissionTimeline> {
    static styles = css`
        @unocss-placeholder;
    `;

    /**
     * List of vertically ordered entries displayed in sequential order.
     * Follows the fields in the {@link SubmissionTimelineEntry} specification.
     */
    @property({ type: Array<SubmissionTimelineEntry> })
    timelineEntries: Array<SubmissionTimelineEntry> = [];

    render() {
        return html`
            <div class="flex flex-col relative">
                ${this.timelineEntries.map((entry) => {
                    return html`
                        <div class="flex py-3">
                            <div
                                class="basis-1/12 text-0.8rem font-thin flex justify-center h-12 items-center"
                            >
                                ${entry.timestamp}
                            </div>
                            <div
                                class="basis-1/16 flex justify-center h-12 items-center"
                            >
                                ${entry.statusBadge}
                            </div>
                            <div class="grow">${entry.submission}</div>
                        </div>
                    `;
                })}
                <div
                    class="absolute bg-accent-contrast w-0.25 h-full z-[-1] ml-11.45%"
                ></div>
            </div>
        `;
    }
}
