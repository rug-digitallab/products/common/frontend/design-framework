import { css, html } from "lit";
import { customElement, property, state } from "lit/decorators.js";

import DLElement from "../../DLElement";
import { Status } from "../../model/StatusData";
import "../../molecule/status-chain/StatusChain";
import "../../molecule/submission-stage/SubmissionStage";

/**
 * Expandable card with submission stages.
 */
@customElement("dl-submission-card")
export default class SubmissionCard extends DLElement<SubmissionCard> {
    static styles = css`
        @unocss-placeholder;
    `;

    /**
     * Name of the submission.
     */
    @property({ type: String })
    submissionName: string = "Unknown Submission";

    /**
     * Is the submission card expanded?
     */
    @state()
    private isOpen: boolean = false;

    /**
     * List of inner submission stages.
     */
    @property({ type: Array<Object> })
    stages: Array<Object> = [];

    handleSubmissionClick() {
        this.isOpen = !this.isOpen;
    }

    render() {
        let bgColor = this.isOpen ? "bg-accent-primary" : "bg-neutral-contrast";

        let fontColor = this.isOpen
            ? "text-neutral-contrast"
            : "text-accent-contrast";

        return html`
        <div
            class="flex flex-col border-accent-primary b-l-6 rounded-md drop-shadow-md"
        >
            <div
                class="h-12 flex ${bgColor} p-4 items-center hover:cursor-pointer w-full"
                @click=${this.handleSubmissionClick}
            >
                <div class="flex flex-grow justify-between pr-6">
                    <div class="font-bold text-small ${fontColor}">
                        ${this.submissionName}
                    </div>
                    <div class="font-thin text-small ${fontColor}">
                        Passed 50/70 tests
                    </div>
                </div>

                <dl-status-chain
                    .statuses=${[
                        Status.Success,
                        Status.Success,
                        Status.Pending,
                    ]}
                ></dl-status-chain>
            </div>
            ${this.isOpen
                ? html`
                      ${this.stages.map((stage) => {
                          return html`${stage}`;
                      })}
                      <div
                          class="bg-accent-primary text-center text-neutral-contrast font-bold text-small p-4"
                      >
                          Expand
                      </div>
                  `
                : ``}
        </div>`;
    }
}
