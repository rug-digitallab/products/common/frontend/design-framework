import { html } from "lit";
import type { Meta, StoryObj } from "@storybook/web-components";
import "./SubmissionCard";

import "../../molecule/submission-stage/SubmissionStage";
import "../../molecule/test-case-grid/TestCaseGrid";

export default {
    title: "Organism/SubmissionCard",
    component: "dl-submission-card",
    tags: ["autodocs"],
} as Meta;

export const Collapsed: StoryObj = {
    args: {
        submissionName: "Submission #1",
        stages: [
            html`
            <dl-submission-stage stageName="Upload">
                <div>Some fancy progress bars, probably.</div>
            </dl-submission-stage>`,
            html`
            <dl-submission-stage stageName="Compile">
                <div>* Compiling Noises *</div>
            </dl-submission-stage>`,
            html`
            <dl-submission-stage stageName="Execution">
                <dl-test-case-grid></dl-test-case-grid>
            </dl-submission-stage>`,
        ],
    },
};

export const Expanded: StoryObj = {
    args: {
        submissionName: "Submission #1",
        stages: [
            html`
            <dl-submission-stage stageName="Upload">
                <div>Some fancy progress bars, probably.</div>
            </dl-submission-stage>`,
            html`
            <dl-submission-stage stageName="Compile">
                <div>* Compiling Noises *</div>
            </dl-submission-stage>`,
            html`
            <dl-submission-stage stageName="Execution">
                <dl-test-case-grid></dl-test-case-grid>
            </dl-submission-stage>`,
        ],
        isOpen: true,
    },
};
