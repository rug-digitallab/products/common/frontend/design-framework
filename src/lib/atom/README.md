# Atoms

![Digital Lab Design Framework Banner](../../../assets/atom_banner.png)

Atoms are the smallest components. They have a unique and well-defined purpose. They can be composed to create more complex components, like *molecules*. We divide atoms in these purposes, each of which has its own subfolder in the hierarchy.

- **Content**: display information from the system, without allowing user interaction. (Labels, status icons...)
- **Input**: receive information from the user that is processed by the system. (Buttons, input fields...)
- **Navigation**: allow the user to move across the application. (Breadcrumbs, navigation tree...)
- **Layout**: organize the structure of the page. (Containers)

If atoms have more than one of these purposes, consider breaking them down or moving them into *molecules*.
