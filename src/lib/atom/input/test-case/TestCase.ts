import { css, html } from "lit";
import { customElement, property } from "lit/decorators.js";

import DLElement from "../../../DLElement";
import { Status, StatusData } from "../../../model/StatusData";

/**
 * Interactive button that displays the state of a testCase.
 */
@customElement("dl-test-case")
export default class TestCase extends DLElement<TestCase> {
    static styles = css`
        @unocss-placeholder;
    `;

    /**
     * Status of the test case.
     */
    @property({ type: Status })
    status: Status = Status.Pending;

    private statusData: Record<Status, StatusData> = {
        [Status.Success]: {
            iconColor: "bg-success-primary",
            borderColor: "border-success-secondary",
        },
        [Status.Failed]: {
            iconColor: "bg-failed-primary",
            borderColor: "border-failed-secondary",
        },
        [Status.Pending]: {
            iconColor: "bg-pending-primary",
            borderColor: "border-pending-secondary",
        },
        [Status.Unknown]: {
            iconColor: "bg-neutral-primary",
            borderColor: "border-accent-primary",
        },
    };

    render() {
        const { iconColor, borderColor } = this.statusData[this.status];
        return html` 
        <button
            class=${`${iconColor} w-8 h-8 rounded-full border-solid ${borderColor} border-4 hover:cursor-pointer`}
            @click=${() => alert("Test Case with state " + this.status)}
        ></button>`;
    }
}
