import type { Meta, StoryObj } from "@storybook/web-components";
import { Status } from "../../../model/StatusData.ts";
import "./TestCase.ts";

export default {
    title: "Atom/Input/TestCase",
    component: "dl-test-case",
    tags: ["autodocs"],
    argTypes: {
        status: {
            options: [Status.Success, Status.Failed, Status.Pending, Status.Unknown],
        },
        statusData: {
            table: { disable: true },
        },
    },
} as Meta;

export const Success: StoryObj = {
    args: {
        status: Status.Success,
    },
};

export const Failed: StoryObj = {
    args: {
        status: Status.Failed,
    },
};

export const Pending: StoryObj = {
    args: {
        status: Status.Pending,
    },
};

export const Unknown: StoryObj = {
    args: {
        status: Status.Unknown
    }
}
