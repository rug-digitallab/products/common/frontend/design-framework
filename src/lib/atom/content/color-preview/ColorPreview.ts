import { css, html } from "lit";
import { customElement, property } from "lit/decorators.js";
import tinycolor from "tinycolor2";

import DLElement from "../../../DLElement";

/**
 * A container to display a single solid color.
 */
@customElement("color-preview")
export default class ColorPreview extends DLElement<ColorPreview> {
    static styles = css`
        @unocss-placeholder;
    `;

    /**
     * Background HEX color.
     */
    @property({ type: String })
    color: string = "#FFFFFF";

    /**
     * Should the preview display the HEX code of the color?
     */
    @property({ type: Boolean })
    showHexCode: boolean = false;

    render() {
        let textColor = tinycolor(this.color).isLight()
            ? "text-black"
            : "text-white";

        return html`
            <div
                style="background-color: ${this.color};"
                class="rounded-2 p-4 border-black border-solid border-2 text-center text-medium ${textColor}"
            >
                ${this.showHexCode ? this.color.toUpperCase() : ""}
            </div>
        `;
    }
}
