import type { Meta, StoryObj } from "@storybook/web-components";
import "./ColorPreview";

export default {
    title: "Atom/Content/ColorPreview",
    component: "color-preview",
    tags: ["autodocs"],
} as Meta;

export const Default: StoryObj = {
    args: {
        color: "#FFFFFF",
        showHexCode: false,
    },
};

export const HexCodeShown: StoryObj = {
    args: {
        color: "#3E98E1",
        showHexCode: true,
    },
};

export const HexCodeHidden: StoryObj = {
    args: {
        color: "#3E98E1",
        showHexCode: false,
    },
};
