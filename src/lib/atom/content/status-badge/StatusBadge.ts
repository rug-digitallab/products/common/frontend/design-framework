import { css, html } from "lit";
import { customElement, property } from "lit/decorators.js";

import DLElement from "../../../DLElement";
import { Status, StatusData } from "../../../model/StatusData";

/**
 * A circular badge that displays the status of an ongoing process.
 */
@customElement("dl-status-badge")
export default class StatusBadge extends DLElement<StatusBadge> {
    static styles = css`
        @unocss-placeholder;
    `;

    /**
     * Status of the circle.
     */
    @property({ type: Status })
    status: Status = Status.Pending;

    private statusData: Record<Status, StatusData> = {
        [Status.Success]: {
            icon: "i-material-symbols:check",
            iconColor: "bg-success-contrast",
            borderColor: "border-success-contrast",
        },
        [Status.Failed]: {
            icon: "i-material-symbols:close",
            iconColor: "bg-failed-contrast",
            borderColor: "border-failed-contrast",
        },
        [Status.Pending]: {
            icon: "i-material-symbols:clock-loader-40",
            iconColor: "bg-pending-contrast",
            borderColor: "border-pending-contrast",
        },
        [Status.Unknown]: {
            icon: "i-material-symbols:question-mark",
            iconColor: "bg-neutral-primary",
            borderColor: "border-neutral-primary",
        },
    };

    render() {
        const { icon, iconColor, borderColor } = this.statusData[this.status];
        return html` 
            <div
                class=${`bg-accent-contrast w-6 h-6 rounded-full flex justify-center items-center ${borderColor} border-4`}
            >
                <div class=${`${icon} ${iconColor} w-75% h-75% rounded-full`}></div>
            </div>`;
    }
}
