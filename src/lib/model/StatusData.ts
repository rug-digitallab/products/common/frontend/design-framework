export enum Status {
    Success = "Success",
    Failed = "Failed",
    Pending = "Pending",
    Unknown = "Unknown"
}

export interface StatusData {
    icon?: string;
    iconColor: string;
    borderColor: string;
}
