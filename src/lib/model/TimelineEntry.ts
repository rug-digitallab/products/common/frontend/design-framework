import StatusBadge from "../atom/content/status-badge/StatusBadge";
import SubmissionCard from "../organism/submission-card/SubmissionCard";

/**
 * Represents a single entry in a submission timeline.
 */
export interface SubmissionTimelineEntry {
    timestamp: String;
    statusBadge: StatusBadge;
    submission: SubmissionCard;
}
