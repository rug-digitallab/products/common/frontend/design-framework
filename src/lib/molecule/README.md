# Molecules

![Digital Lab Design Framework Banner](../../../assets/molecule_banner.png)

Molecules are groupings of *atoms* that serve a particular purpose, even if the responsibility of its atoms are different.

For example, a search box can be composed of an input field, a label and a button. The button and input field correspond to the `input` category, while the label falls under `content`. Nevertheless, these atoms come together to compose a seach box, whose purpose is to let the user search for specific parts within the application.

If molecules become too big, that is, have more than one clear purpose for the user, consider breaking them down or moving them into *organisms*.
