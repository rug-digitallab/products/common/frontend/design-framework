import type { Meta, StoryObj } from "@storybook/web-components";
import { Status } from "../../model/StatusData";
import "./StatusChain";

export default {
    title: "Molecule/StatusChain",
    component: "dl-status-chain",
    tags: ["autodocs"],
} as Meta;

export const AllStates: StoryObj = {
    args: {
        statuses: [
            Status.Success,
            Status.Failed,
            Status.Pending
        ],
    },
};

export const Single: StoryObj = {
    args: {
        statuses: [
            Status.Success,
        ],
    },
};

export const Short: StoryObj = {
    args: {
        statuses: [
            Status.Success,
            Status.Pending
        ],
    },
};

export const Long: StoryObj = {
    args: {
        statuses: [
            Status.Success,
            Status.Success,
            Status.Failed,
            Status.Success,
            Status.Failed,
            Status.Pending
        ],
    },
};
