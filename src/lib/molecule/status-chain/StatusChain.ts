import { css, html } from "lit";
import { customElement, property } from "lit/decorators.js";

import DLElement from "../../DLElement";
import "../../atom/content/status-badge/StatusBadge";
import { Status } from "../../model/StatusData";

/**
 * A chain of StatusBadges.
 */
@customElement("dl-status-chain")
export default class StatusChain extends DLElement<StatusChain> {
    static styles = css`
        @unocss-placeholder;
    `;

    /**
     * List of chain badge states.
     */
    @property({ type: Array<Status> })
    statuses: Array<Status> = [];

    render() {
        return html` 
        <div class="flex space-x-4 w-fit relative">
            ${this.statuses.map(
                (state) =>
                    html`<dl-status-badge
                        status=${state}
                        class="z-2"
                    ></dl-status-badge>`
            )}
            <div class="absolute inset-x-0 bottom-1/2 h-0.5 bg-accent-contrast"></div>
        </div>`;
    }
}
