import { html } from "lit";
import type { Meta, StoryObj } from "@storybook/web-components";
import "./SubmissionStage";

import "../test-case-grid/TestCaseGrid";

export default {
    title: "Molecule/SubmissionStage",
    component: "dl-submission-stage",
    tags: ["autodocs"],
} as Meta;

export const Collapsed: StoryObj = {
    render: () => html` 
    <dl-submission-stage stageName="Collapsed Stage">
        <p>Inner content of the stage.</p>
    </dl-submission-stage>`,
};

export const Expanded: StoryObj = {
    args: {
        stageName: "Expanded Stage",
        isOpen: true,
    },
    
};

export const TestCaseGrid: StoryObj = {
    render: () => html` 
    <dl-submission-stage stageName="Test Case Grid">
        <dl-test-case-grid></dl-test-case-grid>
    </dl-submission-stage>`,
};
