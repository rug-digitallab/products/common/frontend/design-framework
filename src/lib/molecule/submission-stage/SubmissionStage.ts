import { css, html } from "lit";
import { customElement, property, state } from "lit/decorators.js";

import DLElement from "../../DLElement";

/**
 * Inner stage of a submission with configurable content.
 */
@customElement("dl-submission-stage")
export default class SubmissionStage extends DLElement<SubmissionStage> {
    static styles = css`
        @unocss-placeholder;
    `;

    /**
     * Name of the stage.
     */
    @property({ type: String })
    stageName: string = "Stage";

    /**
     * Is the stage content expanded?
     */
    @state()
    private isOpen: boolean = false;

    handleStageClick() {
        this.isOpen = !this.isOpen;
    }

    render() {
        return html` 
        <div class="flex flex-col">
            <div
                class="bg-neutral-contrast h-12 flex items-center justify-between p-4 hover:cursor-pointer"
                @click=${this.handleStageClick}
            >
                <div class="font-bold">${this.stageName}</div>
                <div>Completed</div>
            </div>
            ${this.isOpen
                ? html`
                      <div class="bg-neutral-primary p-8">
                          <slot></slot>
                      </div>
                  `
                : ""}
        </div>`;
    }
}
