import prand from "pure-rand";

import type { Meta, StoryObj } from "@storybook/web-components";
import { Status } from "../../model/StatusData";
import "./TestCaseGrid";

export default {
    title: "Molecule/TestCaseGrid",
    component: "dl-test-case-grid",
    tags: ["autodocs"],
} as Meta;

const rng = prand.xoroshiro128plus(12345);

const generateRandomListOfTestCases = (numberOfTestCases: number) => {
    return Array.from({ length: numberOfTestCases }, () => {
        const possibleValues: Status[] = [
            Status.Success,
            Status.Failed,
            Status.Pending,
        ];
        return possibleValues[
            prand.unsafeUniformIntDistribution(
                0,
                possibleValues.length - 1,
                rng
            )
        ];
    });
};

export const Small: StoryObj = {
    args: {
        testCases: generateRandomListOfTestCases(16),
    },
};

export const Medium: StoryObj = {
    args: {
        testCases: generateRandomListOfTestCases(52),
    },
};

export const Large: StoryObj = {
    args: {
        testCases: generateRandomListOfTestCases(208),
    },
};
