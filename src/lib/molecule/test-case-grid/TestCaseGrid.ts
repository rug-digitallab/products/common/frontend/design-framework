import { css, html } from "lit";
import { customElement, property } from "lit/decorators.js";

import DLElement from "../../DLElement";
import "../../atom/input/test-case/TestCase";
import { Status } from "../../model/StatusData";

/**
 * Dynamic grid of test cases.
 */
@customElement("dl-test-case-grid")
export default class TestCaseGrid extends DLElement<TestCaseGrid> {
    static styles = css`
        @unocss-placeholder;
    `;

    /**
     * List of test cases to display.
     */
    @property({ type: Array<Status> })
    testCases: Array<Status> = [Status.Success, Status.Failed, Status.Pending];

    render() {
        return html`
        <div
            class="flex flex-wrap space-x-1 space-y-1 items-center justify-center"
        >
            ${this.testCases.map((testCase) => {
                return html`<dl-test-case
                    status=${testCase}
                ></dl-test-case>`;
            })}
        </div>`;
    }
}
