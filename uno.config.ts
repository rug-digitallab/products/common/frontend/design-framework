// uno.config.ts
import {
    defineConfig,
    presetAttributify,
    presetUno,
    presetIcons,
    presetWebFonts,
} from "unocss";
import { tokens } from "./src/style/themis-tokens";

export default defineConfig({
    presets: [
        presetAttributify(),
        presetUno(),
        presetIcons(),
        presetWebFonts({
            provider: 'google',
            fonts: {
                sans: [
                    {
                        name: "Noto Sans",
                        weights: Array.from({ length: 9 }, (_, i) => `${(i + 1) * 100}`),
                        italic: true
                    }
                ]
            }
        })
    ],
    preflights: [
        {
            getCSS: ({ theme }) => `
            *,
            ::before,
            ::after {
                box-sizing: border-box;
                border-width: 0;
                border-style: solid;
                border-color: theme('borderColor.DEFAULT', currentColor);
                font-family: sans-serif;
            }
            :root {
                --color-neutral-primary: ${tokens.NEUTRAL};
                --color-neutral-contrast: ${tokens.NEUTRAL_CONTRAST};
                --color-accent-primary: ${tokens.ACCENT};
                --color-accent-contrast: ${tokens.ACCENT_CONTRAST};
                --color-success-primary: ${tokens.SUCCESS};
                --color-success-secondary: ${tokens.SUCCESS_SECONDARY};
                --color-success-contrast: ${tokens.SUCCESS_CONTRAST};
                --color-failed-primary: ${tokens.FAILED};
                --color-failed-secondary: ${tokens.FAILED_SECONDARY};
                --color-failed-contrast: ${tokens.FAILED_CONTRAST};
                --color-pending-primary: ${tokens.PENDING};
                --color-pending-secondary: ${tokens.PENDING_SECONDARY};
                --color-pending-contrast: ${tokens.PENDING_CONTRAST};

                --font-size-large: ${tokens.LARGE};
                --font-size-medium: ${tokens.MEDIUM};
                --font-size-small: ${tokens.SMALL};
            }
          `,
        },
    ],
    theme: {
        colors: {
            neutral: {
                primary: "var(--color-neutral-primary)",
                contrast: "var(--color-neutral-contrast)",
            },
            accent: {
                primary: "var(--color-accent-primary)",
                contrast: "var(--color-accent-contrast)",
            },
            success: {
                primary: "var(--color-success-primary)",
                secondary: "var(--color-success-secondary)",
                contrast: "var(--color-success-contrast)",
            },
            failed: {
                primary: "var(--color-failed-primary)",
                secondary: "var(--color-failed-secondary)",
                contrast: "var(--color-failed-contrast)",
            },
            pending: {
                primary: "var(--color-pending-primary)",
                secondary: "var(--color-pending-secondary)",
                contrast: "var(--color-pending-contrast)",
            },
        },
        fontSize: {
            large: "var(--font-size-large)",
            medium: "var(--font-size-medium)",
            small: "var(--font-size-small)",
        },
    },
});
