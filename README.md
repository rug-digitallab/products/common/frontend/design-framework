# Digital Lab Design Framework

## A modular UI component library to be used across *Digital Lab* user-facing products.

![Digitial Lab Design Framework Banner](assets/banner.png)

Built with:

- [<img src="assets/lit.png" width=30/> Lit](https://lit.dev/docs/) as the underlying frontend framework, based on web component technology.
- [<img src="assets/unocss.png" width=30/> UnoCSS](https://unocss.dev/) for utility-based styling.
- [<img src="assets/storybook.png" width=30/> Storybook](https://storybook.js.org/) to organize and manage the component library.
- [<img src="assets/chromatic.png" width=30/> Chromatic](https://www.chromatic.com/) to perform visual testing & review.

## Prerequisites

Currently, this project uses [`pnpm`](https://pnpm.io/motivation) as the package manager and `nvm` as the Node version manager. Only installing `nvm` should be sufficient to get going.

## Usage

1. Run `nvm install` to install and activate the required Node version.

1. Run `corepack enable` to active the `pnpm` package manager

1. Run `pnpm install` to download the required dependencies.

1. Run `pnpm storybook` to launch the Storybook workshop. Internally, this runs two commands concurrently:
   - `cem analyze`: helper tool to produce custom element metadata for use in Storybook
   - `storybook dev -p 6006`: the Storybook server.
   - `unocss \"./src/**/*.ts\" -o src/styles/uno.css --watch`: watches the UnoCSS classes and generates an on-the-fly stylesheet under `src/styles/uno.css`. This stylesheet is then imported in the storybook preview at `.storybook/prewiew.ts`. This stylesheet is never pushed.

1. Optionally, run `pnpm dev` to launch the development server. This is used to test UI components and pages outside of the Storybook workshop.

## Project Structure

- `.storybook/`: Storybook configuration files.
- `assets/`: Component independent files, like the banner at the top of this README.
- `src/lib`: The component library itself, including stories.
- `uno.config.ts`: UnoCSS configuration for themes, font families, etc.
